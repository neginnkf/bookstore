var express = require("express");
var passwordHash = require('password-hash');
var bodyParser = require('body-parser');
var db = require('db');
var unixTime = require('unix-time');
var request = require('request');
var session = require('express-session');
var unixTime = require('unix-time');
var Recaptcha = require('express-recaptcha');
var recaptcha = new Recaptcha('6LfG40YUAAAAAD4Utkd_ugU1_kl-FmEvP56LeKCP', '6LfG40YUAAAAABFbssss6n4lImXpoOtBKBKIYwzn');
var app = express();
app.set('trust proxy', 1);// trust first proxy
app.use(session({secret: 'keyboard cat', cookie: {maxAge: 172800000}}));


//var mainAddress = "http://165.227.245.6:9000/";
var mainAddress = "http://localhost:9000/";

app.use(express.static(__dirname + "/views"));
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json());


app.get('/', function (request, response, next) {


    if (request.session.login != undefined) {
        response.render('header', {'title': 'کتاب رسان'});
    } else {
        response.render('login', {captcha: recaptcha.render(), mainAddress: mainAddress});
    }
});
app.get('/login', function (request, response, next) {
    //var hashedPassword = passwordHash.generate('admin_ketabresan@1396');
    response.render('login', {captcha: recaptcha.render(),mainAddress:mainAddress});
});


app.post('/login', function (request, response, nexSSSt) {

    var username = request.body['username'];
    var password = request.body['password'];
    recaptcha.verify(request, function (error, data) {
        if (!error) {
            if (username.length < 3) {
                response.render('login', {
                    'err': "نام کاربری بسیار کوتاه است",
                    captcha: recaptcha.render(),
                    mainAddress: mainAddress
                });
                return;
            } else if (password.length < 5) {
                response.render('login', {
                    'err': "کلمه عبور بسیار کوتاه است",
                    captcha: recaptcha.render(),
                    mainAddress: mainAddress
                });
                return;
            }

            var sql = "select * from str_user where username=? or  email=? ";
            db.query(sql, [username, username], function (err, result) {
                if (err) throw  err;
                if (result[0] != undefined) {
                    var password_hash = result[0]['password'];

                    if (result[0] != null && passwordHash.verify(password, password_hash) && result[0]['role'] == '1') {
                        request.session.login = result[0]['username'];
                        response.render('header', {'title': 'کتاب رسان', mainAddress: mainAddress});
                        console.log("LOP");

                    } else {
                        response.render('login', {
                            'err': 'نام کاربری و یا کلمه عبور اشتباه است',
                            captcha: recaptcha.render(),
                            mainAddress: mainAddress
                        });
                    }
                }else{
                    response.render('login', {
                        'err': 'نام کاربری و یا کلمه عبور اشتباه است',
                        captcha: recaptcha.render(),
                        mainAddress: mainAddress
                    });
                }

            });
        } else {
            response.render('login', {
                'err': 'لطفا کپچا را تایید نمایید',
                captcha: recaptcha.render(),
                mainAddress: mainAddress
            });
        }
    });
});
app.get('/showbooks', function (request, response, next) {
    if (request.session.login != undefined) {
        if (request.query['current'] == undefined) {
            var current_page = 0;
        } else {
            var current_page = request.query['current'];
        }
        var sql = "select * from books";
        db.query(sql, function (err, data) {
            if (err) throw err;
            if (data[0] != undefined) {
                response.render('booklist', {data: data, mainAddress: mainAddress, current: current_page});
            }
        });
    } else {
        response.render('login', {captcha: recaptcha.render(), mainAddress: mainAddress});
    }
});
app.get('/refreshbooks', function (request, response, next) {
    if (request.session.login != undefined) {
        var any_page_show = 20;
        var current_page = request.query['current_page'];
        var sql = "select count(*) as num from books ";
        db.query(sql, function (err, count) {
            if (err) throw err;

            var total_row = count[0]['num'];

            var sql = "select * from books  LIMIT ?,? ";
            db.query(sql, [any_page_show * current_page, any_page_show], function (err, data) {
                if (err) throw err;
                if (data[0] != undefined) {
                    //   console.log(data);
                    response.json({
                        data: data,
                        current_page: current_page,
                        total_row: total_row,
                        any_page_show: any_page_show,
                        mainAddress: mainAddress
                    });
                }
            });

        });
    } else {
        response.render('login', {captcha: recaptcha.render(), mainAddress: mainAddress});
    }


});
app.get('/updatePrice', function (request, response, next) {
    if (request.session.login != undefined) {
        console.log(request.query);
        var id = request.query['id'];
        var current = request.query['page'];
        var sql = "select * from books where id=?";
        db.query(sql, [id], function (err, data) {
            if (err) throw err;
            if (data[0] != undefined) {
                console.log(data);
                response.render('price', {data: data, mainAddress: mainAddress, current: current});
            }
        });
    } else {
        response.render('login', {captcha: recaptcha.render(), mainAddress: mainAddress});
    }

});
app.post('/setprice', function (request, response, next) {
    if (request.session.login != undefined) {
        var id = request.body['id'];
        var price = request.body['price'];
        var sql = "update books set Price=? where id=?";
        db.query(sql, [price, id], function (err, data) {
            if (err) throw err;
        });
    } else {
        response.render('login', {captcha: recaptcha.render(), mainAddress: mainAddress});
    }
});

app.get('/deletebook',function (request,response,next) {
   if(request.session.login!= undefined){
       var id = request.query['id'];
       var current = request.query['page'];
       var sql="delete from books where id=?";
       db.query(sql,[id],function (err,data) {
           if(err) throw err;

       });

   }
});


app.listen("9000");
console.log("Server Running!!!");